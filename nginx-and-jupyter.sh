#!/bin/bash

# NGINX
systemctl start nginx
systemctl enable nginx
systemctl status nginx

# JUPYTER
jupyter notebook --config=/etc/jupyter/jupyter_notebook_config.py
