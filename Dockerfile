# A simulation of a virtual Ubuntu machine running Jupyter Notebook server
# proxied via nginx.

######################
# The image workflow #
######################

# Build the image
#
# The command to build the image based on the current `Dockerfile` is:
#    docker build . -t nginx-jupyter-ubuntu

# Run the container
#
# In order to run the image use the following command
#     docker run -p 8080:80 --name nginx-jupyter-ubuntu nginx-jupyter-ubuntu
#  
# where:
#   * 8080 is the port exposed outside of the docker image (on a host machine)
#   * 80 is the TCP port inside the container on which nginx proxies

# Interactive shell
#
# In order to enter the image interactively use the following command:
#   * docker exec -it nginx-jupyter-ubuntu /bin/zsh

# Cleanup
#
# In order to stop and delete container, as well as delete the image,
# use the following one-line bash command:
#    docker stop $(docker ps -a | grep 'nginx-jupyter-ubuntu' | awk '{ print $1 }') && docker rm $(docker ps -a | grep 'nginx-jupyter-ubuntu' | awk '{ print $1 }') && docker rmi nginx-jupyter-ubuntu


#######################
# Dockerfile contents #
#######################

# This docker image is based on Ubuntu, because most the virtual machines use
# this Linux distribution.
FROM ubuntu:focal

# Update the repositories
RUN apt-get update

# Systemctl is not installed by default in the docker image.
# we will install it for concenience, as well as the vim.
# `nginx` is of course required as it is a part of this example.
RUN apt-get install vim nginx systemctl -y

# Note: Nginx has to be run with a wrapper script (see the bottom of the Dockerfile).
# Running systemctl commands with "RUN" is pointless because temporary images
# are created for that, which are immediately destroyed and nginx's service
# is not persistent, see: https://docs.docker.com/config/containers/multi-service_container/
# Using multiple CMDs also doesn't help for this issue.

# Prepare Jupyter Notebook configuration for nginx
# Copy the configuration from the current directory (repository) inside a docker image
COPY jupyter-notebook.conf /etc/nginx/sites-available/jupyter-notebook.conf

# Remove the default `nginx` configuration
RUN rm /etc/nginx/sites-enabled/default

# Enable Jupyter's site (nginx configuration) by linking it to the added config
RUN ln -s /etc/nginx/sites-available/jupyter-notebook.conf /etc/nginx/sites-enabled/jupyter-notebook.conf

# Pip is needed to install Jupyter Notebook
RUN apt-get install python3-pip -y

# Install ZSH for convenience
RUN apt-get install zsh -y

# Jupyter Notebook instance will be run later on
# Firstly, we need to install the Jupyter Notebook
RUN pip install jupyter notebook

# Use custom Jupyter Notebook settings
RUN mkdir /etc/jupyter
COPY jupyter_notebook_config.py /etc/jupyter/jupyter_notebook_config.py

# We make sure that the nginx service is up and running.
# Jupyter Notebook has to be run when system (docker) starts.
# For both of these things we have one script that will be run
# each time the docker container starts.
COPY nginx-and-jupyter.sh nginx-and-jupyter.sh

# Executable permission has to be added to the script file
RUN ["chmod", "+x", "nginx-and-jupyter.sh"]

# Run the script for starting nginx service, as well as the Jupyter Notebook server
CMD ./nginx-and-jupyter.sh
